package com.codistan.java.lesson10;

public class SimpleCalculatorRunner {
	public static void main(String args[]) {
		SimpleCalculator myCalc = new SimpleCalculator();
		myCalc.addNumbers(5.0, 20.0);
		myCalc.addNumbers(11.1, 0.4);
		myCalc.substractNumbers(10.0, 5.0);
		myCalc.substractNumbers(10.5, 5);

		double tempNum = myCalc.addNumbers(2, 2);
		System.out.println(tempNum);
		byte i = (byte) tempNum; // expelecit casting
		System.out.println(i);
		myCalc.addToMemory(myCalc.addNumbers(2, 3));

//		myCalc.addToMemory(myCalc.addNumbersAndSaveToTheMemory(2, 3)); // m becomes 5
//		System.out.println(myCalc.getM()); // prints m
//		myCalc.mPlus(4); // add 4 to m resulting to 9
//		System.out.println(myCalc.getM()); // prints m
//		myCalc.mPlus(5); // adds 5 to 9
//		System.out.println(myCalc.getM()); // prints m
//
//		System.out.println(myCalc.addNumbers(1, 2, 3));
//
//		System.out.println(myCalc.addNumbers(100, 100));
//		System.out.println(myCalc.addNumbers(0, 1001));

		System.out.println(myCalc.addNumbers(2, 20));// testing a positive test case with the first ocject
		SimpleCalculator myCalc2 = new SimpleCalculator(0, 10);
		System.out.println(myCalc2.addNumbers(2, 9));// testing positive test case
		// System.out.println(myCalc2.addNumbers(2, 20));testing negative test case
		//System.out.println(myCalc.circleArea(2));
		System.out.println(myCalc.pi);

		// static method it can only access static

		SmartCalculator mySmartCalc = new SmartCalculator();
		System.out.println(mySmartCalc.squareArea(3));

		System.out.println(mySmartCalc.addNumbers(2, 2, 2, 2));

	}
}
