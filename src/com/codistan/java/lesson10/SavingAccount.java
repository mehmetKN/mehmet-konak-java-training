package com.codistan.java.lesson10;

/*2-Please create a SavingAccount Class as follows: 
a. You can open a saving account at least with $10k (this should be handled by the constructor)
b. If your balance becomes less than $10K at any point in time, the bank will charge you $35. +
c. If your balance becomes more than $20K, the bank will add 5% of your balance to the balance. *
d. Once your balance becomes less than $20K, the bank will charge you 10%. *
e. If your balance becomes less than $10K anytime, the bank will charge you 20% and you will not able to do more transaction anymore. 
f. Every transaction should be recorded and when we ask for the transaction history, it will print the transaction including all the charges that bank did or all the earnings so that user will know.
*/

public class SavingAccount extends BankAccount {
	
	public double minimumSavingAccountBalance = 10000;
	public double interestSavingAccountBalance=20000;

	public SavingAccount(String accountNo, double balance, String userSSN) {
		super(accountNo, balance, userSSN);
		// TODO Auto-generated constructor stub
	}
	
	

	public SavingAccount(String accountNo, double balance, String userSSN, double minimumSavingAccountBalance) {
		super(accountNo, balance, userSSN);
		this.minimumSavingAccountBalance = minimumSavingAccountBalance;
	}



	@Override
	public void withdrawCash(double amount) {
		// TODO Auto-generated method stub
		blockSavingAccount(amount);
		checkLessSavingBalance(amount);
		cutInterest(amount);
		super.withdrawCash(amount);
	}
	
	
	
	@Override
	public void transferFundsInternally(double amount, String accountNumber) {
		// TODO Auto-generated method stub
		checkLessSavingBalance(amount);
		super.transferFundsInternally(amount, accountNumber);
	}



	public void checkLessSavingBalance(double amount) {
		if (this.checkBalance()-amount< this.minimumSavingAccountBalance) {
			this.chargeFee(35);
			System.out.println("Low account fee has been charged");
			this.chargeFee(checkBalance()*(0.20));
			System.out.println("% 20 extra low account fee has been charged");
			
			}
		}
		
		public boolean blockSavingAccount(double amount) {
			if(this.checkBalance()-amount >= this.minimumSavingAccountBalance) {
				return true;
			}else {
				
				System.out.println("Your balance can not be less than " + minimumSavingAccountBalance + " Your account is blocked") ;
				return false;
				
				
			}
		}
		
	
		public void addInterest(double interest) {
			this.setBalance(this.getBalance()+interest);
		}
		
	public void addInterestSavingAccount(double amount) {
		if(this.checkBalance()+amount > interestSavingAccountBalance) {
			this.addInterest(this.checkBalance()*(0.05));
			System.out.println("% 5 interest has been added");
			
			
		}

}

	@Override
	public void depositCash(double amount) {
		// TODO Auto-generated method stub
		addInterestSavingAccount(amount);
		super.depositCash(amount);
	}
	
	
	
	@Override
	public void depositCheck(double amount, String accountNo, String routingNo) {
		addInterestSavingAccount(amount);
		super.depositCheck(amount, accountNo, routingNo);
	}



	public void cutInterest(double amount) {
		if(this.checkBalance()-amount<interestSavingAccountBalance) {
			this.chargeFee(this.checkBalance()*(0.10));
			System.out.println("10 % fee has been charged");
		}
		
		
		
	}
	@Override
	public void addTransaction(double amount) {
		super.addTransaction(amount);
	}
}

