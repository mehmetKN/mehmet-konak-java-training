package com.codistan.java.lesson10;

public class SmartCalculator extends SimpleCalculator {

	/*
	 * Please add a method called squareArea() to the SimpleCalculator that
	 * calculates the area of a square.
	 */

	public SmartCalculator() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SmartCalculator(double m, double min, double max) {
		super(m, min, max);
		// TODO Auto-generated constructor stub
	}

	public SmartCalculator(double min, double max) {
		super(min, max);
		// TODO Auto-generated constructor stub
	}

	public double squareArea(double a) {
		return a * a;
	}

	public double addNumbers(double a, double b) {// overriding
		double total = a + b;
		System.out.println("The total of " + "and" + b + "is" + total);
		return total;
	}

	public double addNumbers(double a, double b, double c, double d) {// overloading
		return a + b + c + d;
	}

	/*
	 * Please add a method called circleArea() to the SimpleCalculator that
	 * calculates the area of a circle.
	 */
//		 
//		public double circleArea(double r) {
//			return pi*(r*r);
//		}
//	
	
	 

}
