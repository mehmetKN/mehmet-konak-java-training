package com.codistan.java.lesson10;

/*
1- if the balance gets lower than $1500 at any point, you will be charged for $15
2- You can not withdraw more than $10,000 per transaction.
3- If the balance is more $10,000, send a message to IRS.
4- If the balance goes lower than zero, you will be charged for $35.
5- The balance can not go less than -500.

*/

public class CheckingAccount extends BankAccount{

	public double minimumBalanceLimit = 1500;
	public double maximumTransactionAmountLimit = 10000;
	public double accountBalanceMinimumThreshold = (-500); 

	public CheckingAccount(String accountNo, double balance, String userSSN) {
		super(accountNo, balance, userSSN);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void withdrawCash(double amount) {
		if (this.checkMaximumTransactionAmount(amount) && this.checkAccBalMinThreshold(amount)) {
			// TODO Auto-generated method stub
			checkMinimumBalanceRule(amount);
			checkNegativeAccountBalance(amount);
			super.withdrawCash(amount);
		}
	}

	@Override
	public void transferFundsInternally(double amount, String accountNumber) {
		if (this.checkMaximumTransactionAmount(amount)) {
			// TODO Auto-generated method stub
			checkMinimumBalanceRule(amount);
			checkNegativeAccountBalance(amount);
			super.transferFundsInternally(amount, accountNumber);
		}
	}

	public void checkMinimumBalanceRule(double amount) {
		if (this.checkBalance() - amount < this.minimumBalanceLimit) {
			this.chargeFee(15);
		}
	}

	public boolean checkMaximumTransactionAmount(double amount) {
		if (amount <= this.maximumTransactionAmountLimit) {
			return true;
		} else {
			System.out.println(
					"You are not allowed to use more than " + this.maximumTransactionAmountLimit + " per transaction!");
			return false;
		}
	}
	
	@Override
	public void depositCash(double amount) {
		// TODO Auto-generated method stub
		super.depositCash(amount);
		notifyIRSIfNecessary();
	}

	@Override
	public void depositCheck(double amount, String accountNo, String routingNo) {
		// TODO Auto-generated method stub
		super.depositCheck(amount, accountNo, routingNo);
		notifyIRSIfNecessary();
	}

	public void notifyIRSIfNecessary() {
		if(this.checkBalance() > 10000) {
		System.out.println("Hey IRS, The following account has a balance more than $10K: " + this.getAccountNo());
		}
	}
	
	public void checkNegativeAccountBalance(double amount) {
		if (this.checkBalance() - amount < 0) {
			this.chargeFee(35);
		}
	}
	
	public boolean checkAccBalMinThreshold(double amount) {
		if( this.checkBalance() - amount > this.accountBalanceMinimumThreshold ) {
			return true; 
		} else {
			System.out.println("Your final balance can not be less than -$500 !");
			return false; 
		}
	}
}