package com.codistan.java.lesson10;

public class CheckingAccountRunner {

	public static void main(String[] args) {
		CheckingAccount myCheckingAccount = new CheckingAccount("00002", 500, "222333444");
		myCheckingAccount.withdrawCash(50);
		myCheckingAccount.checkBalance();
		myCheckingAccount.depositCash(15000);
		myCheckingAccount.checkBalance(); 
		myCheckingAccount.withdrawCash(11000);
		myCheckingAccount.checkBalance();
		myCheckingAccount.withdrawCash(16000);
		myCheckingAccount.checkBalance();
		myCheckingAccount.withdrawCash(9000);
		myCheckingAccount.withdrawCash(7000);
		myCheckingAccount.checkBalance();
		myCheckingAccount.showTransactions();
		
	//	System.out.println(args[0]);

}

}
