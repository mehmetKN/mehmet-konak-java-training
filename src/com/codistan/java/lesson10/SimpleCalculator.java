package com.codistan.java.lesson10;

public class SimpleCalculator {

	// Class assignment: please create another constructor that will set m only.
	double m = 0.0;
	double maxInputLimit = 1000;
	double minInputLimit = 0;
	final double pi = 3.14; // once we assing a value, it never

	public SimpleCalculator(double min, double max) {
		this.maxInputLimit = max;
		this.minInputLimit = min;
	}

	public SimpleCalculator() {
		super();
	}
	/*
	 * Class assignment: please create a constructor that will set the fields m,
	 * minInputLimit, maxInputLimit.
	 */

	public SimpleCalculator(double m, double min, double max) {
		this(min, max);
		this.m = m;
//		minInputLimit=min;
//		maxInputLimit= max;
	}

	public void inputValidation(double input) {
		if (!(input <= maxInputLimit && input >= minInputLimit)) {
			System.out.println("Invalid");
			System.exit(0);
		}
	}

	public double addNumbers(double num1, double num2) {// this scope this is coverage
		// System.out.println(num1+num2);
		inputValidation(num1);
		inputValidation(num2);
		return num1 + num2;

	}

	public double addNumbers(double num1, double num2, double num3) {
		return num1 + num2 + num3;

	}

	public double substractNumbers(double num3, double num4) {
		return num3 - num4;
	}

	public double divdedNumbers(double num3, double num4) {
		double result = 0.0;
		if (num3 != 0) {
			result = num3 / num4;
		} else {
			System.out.println("0 is not acceptable for the second number ");
		}
		return result;
	}

	public double multiplyNumber(double num5, double num6) {
		return num5 * num6;
	}

	public void addToMemory(double inputForMemory) {
		m = inputForMemory;
	}

	public double getM() {
		return m;
	}

	public void setM(double m) {
		this.m = m;// current object
	}

//	public void setM(double inputForMemory) {
//		m=inputForMemory;
//		
//	}
//	
//	public double getM() {
//		return m;
//	}

	public double addNumbersAndSaveToTheMemory(double a, double b) {
		double total = addNumbers(a, b);
		setM(total);
		return total;
	}

	public void printMemory() {
		getM();
	}

	public void mPlus(double currentValue) {
		// setM(getM()+currentValue);
		double total = getM() + currentValue;
		setM(total);
	}

	public void mMinus(double currentValue) {
		double total = getM() - currentValue;
		setM(total);
	}

	public void mReset() {
		setM(0);
	}

}



