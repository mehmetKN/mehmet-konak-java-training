package com.codistan.java.lesson10;

import java.util.ArrayList;

public class BankAccount {
	
	private String accountNo; 
	private double balance; 
	private String userSSN;
	private ArrayList<Double> transactionHistory = new ArrayList<>(); 
	
	public BankAccount(String accountNo, double balance, String userSSN) {
		this.accountNo = accountNo;
		this.balance = balance;
		this.userSSN = userSSN;
	}

	public String getAccountNo() {
		return accountNo;
	}
	
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getUserSSN() {
		return userSSN;
	}
	public void setUserSSN(String userSSN) {
		this.userSSN = userSSN;
	}
	
	public double checkBalance() {
		System.out.println( "Current Balance: " + this.getBalance() );
		return this.getBalance();
	}
	
	public void depositCash(double amount) {
//		this.setBalance(this.getBalance()+amount);
		double totalAmount = getBalance() + amount;
		setBalance(totalAmount);
		this.addTransaction(amount);
	}
	
	public void depositCheck(double amount, String accountNo, String routingNo) {
//		this.setBalance(this.getBalance()+amount);
		double totalAmount = getBalance() + amount;
		setBalance(totalAmount);
		this.addTransaction(amount);
	}
	
	public void withdrawCash(double amount) {
		this.setBalance(this.getBalance()-amount);
		this.addTransaction((amount)*(-1));
	}
	
	public void transferFundsInternally(double amount, String accountNumber ) {
		this.setBalance(this.getBalance()-amount);
		this.addTransaction(amount*(-1));
	}
	
	public void addTransaction(double amount) {
		this.transactionHistory.add(amount);
	}
	
	public void showTransactions() {
		for (Double double1 : transactionHistory) {
			System.out.println("Transaction:" + double1);
		}
	}
	
	public void chargeFee(double fee) {
		this.setBalance(this.getBalance()-fee);
	}
}
