package com.codistan.java.lesson10;

public class BankAccountRunner {

	public static void main(String[] args) {
		BankAccount myBankAccount = new BankAccount("000001", 0, "111222333"); 
		myBankAccount.depositCash(300);
		myBankAccount.checkBalance(); 
		
		myBankAccount.withdrawCash(110);
		myBankAccount.checkBalance();

	}

}
