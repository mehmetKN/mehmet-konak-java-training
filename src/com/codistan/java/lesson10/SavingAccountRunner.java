package com.codistan.java.lesson10;

public class SavingAccountRunner {

	public static void main(String[] args) {
		SavingAccount mySavingAccount = new SavingAccount("02020202", 10000, "222233333");
		
		mySavingAccount.withdrawCash(100);
		mySavingAccount.checkBalance();
		mySavingAccount.depositCash(18000);
		mySavingAccount.checkBalance();
		mySavingAccount.withdrawCash(7265.6);
		mySavingAccount.addTransaction(50);
		mySavingAccount.checkBalance();
		mySavingAccount.showTransactions();

		
			}

}
