package com.codistan.java.loopAssignments;

public class WeirdLoop {

	public static void main(String[] args) {
		
		int i, j;
		
		
		for (i=100, j=0; !(i==j); i-=5, j+=5  )
			System.out.print(i + ", " + j + ", ");

		if (i == j) {
			System.out.println(i);
		}

	}

}
