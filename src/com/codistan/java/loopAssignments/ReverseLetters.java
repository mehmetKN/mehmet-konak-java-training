package com.codistan.java.loopAssignments;

public class ReverseLetters {

	public static void main(String[] args) {
		System.out.println("Hello World this is QA class and we are doing great!");
		String text = new String("Hello World this is QA class and we are doing great!");
		System.out.println("Reversed string is:");
	    for (int i = (text.length() - 1); i >= 0; i--) {
	    System.out.print(text.charAt(i));

	}
	}
}
