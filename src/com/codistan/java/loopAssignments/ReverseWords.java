package com.codistan.java.loopAssignments;

public class ReverseWords {

	public static void main(String[] args) {
		
		
		String reverseWords = "Hello World this is QA class Number 1 and we are doing great!";
		StringBuilder reverseString = new StringBuilder();
		String[] words = reverseWords.split(" ");       		 
		
		 for (String word : words)
		{
		    String reverseWord = new StringBuilder(word).reverse().toString();      
		    reverseString.append(reverseWord + " ");                                
		}
		 
		System.out.println( reverseString.toString().trim() );  
		

	}

}
