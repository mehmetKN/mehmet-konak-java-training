package com.codistan.java.loopAssignments;

import java.util.Scanner;

public class GradeCalculator {
	
//	Please create an application class called GradeCalculator in com.codistan.java.loopassignments package. 
//	This application will ask for Student Name and then it will ask for the exam points he got and the application will 
//	calculate his/her grade based on the points as following and print it to the console; if the grade is greater than 85 the grade will be �A�, 
//	greater than 75 will be �B�, greater than 65 will be �C�, greater than 55 will be �D� and 
//	55 and less will be �F�.

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("What is your name?");
		String name = scan.next();
		
		System.out.println("What is your grade?");
		int grade = scan.nextInt();
		
		if (grade>=85)
		{ System.out.println(name + "'s grade is"  + " A");}
		else if (grade>=75) {
			{ System.out.println(name + "'s grade is"  + " B");}
		}else if (grade>=65) {
			{ System.out.println(name + "'s grade is"  + " C");}
		}else if (grade >=55) {
			{ System.out.println(name + "'s grade is"  + " D");}
		}else { System.out.println(name + "'s grade is"  + " F");}
	}

}
