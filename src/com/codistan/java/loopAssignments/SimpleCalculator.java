package com.codistan.java.loopAssignments;

import java.util.Scanner;

public class SimpleCalculator {

//	Press 1 for addition, Press 2 for subtraction, Press 3 for multiplication, Press 4 for division. After that it will say this; 
//			please enter the first number, after getting the first user input, it will say please enter the second number, 
//			after getting this number the program should do the calculation based on the user inputs.
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Press 1 for addition");
		System.out.println("Press 2 for subtraction");
		System.out.println("Press 3 for multiplication");
		System.out.println("Press 4 for division");
		int calculator = scan.nextInt();
		
		System.out.println("Please enter your first number");
		int number1 = scan.nextInt();
		System.out.println("Please enter your second number");
		int number2 = scan.nextInt();
		
		if (calculator ==1) {
			System.out.println(number1 + number2);
			}
		else if (calculator==2) {
			System.out.println(number1-number2);
		}else if (calculator==3) {
			System.out.println(number1*number2);
		}else {System.out.println(number1/number2);}
		
		
		
	}

}
