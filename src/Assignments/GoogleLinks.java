package Assignments;

import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleLinks {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter the search term.");
		String searchTerm = scanner.nextLine();
		
		
		WebDriver driver = new ChromeDriver(); 
		driver.get("http://www.google.com");
		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys(searchTerm);
        element.submit();
        
        List<WebElement> findElements = driver.findElements(By.xpath("//cite[@class='iUh30']"));

               
        for (int i = 0; i < 9; i++) {
        	if(findElements.get(i).getText().equals("")) {
        		continue;
        	}
			System.out.println(findElements.get(i).getText());
		}

	}

}
