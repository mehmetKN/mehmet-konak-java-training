package Assignments;

import java.util.Scanner;

public class EmployeeLoyaltyApp {

	public static void main(String[] args) {
		Scanner myScan = new Scanner(System.in);
		System.out.println("How many years?");
		int numberOfYearsWorked = myScan.nextInt();
		System.out.println("Are you manager? Type yes or no");
		Scanner myScan2 = new Scanner(System.in);
		String manager = myScan2.nextLine().trim().toLowerCase();
		System.out.println();

		if (numberOfYearsWorked < 20 && manager.equals("no")) {
			System.out.println(
					"This employee got " + ((numberOfYearsWorked / 3) + (numberOfYearsWorked / 10)) + " golds");
		}

		else if (numberOfYearsWorked < 20 && manager.equals("yes")) {
			System.out.println(
					"This employee got " + (((numberOfYearsWorked / 3) + (numberOfYearsWorked / 10)) * 3) + " golds");
		} else if (numberOfYearsWorked >= 20 && manager.equals("no")){
			System.out.println(
					"This employee got " + ((((numberOfYearsWorked / 3) + (numberOfYearsWorked / 10)) * 2)-7) + " golds");
		} else if (numberOfYearsWorked >= 20 && manager.equals("yes")) {
			System.out.println("This employee got "
					+ (((((numberOfYearsWorked / 3) + (numberOfYearsWorked / 10)) * 3)*2)-21) + " golds");
		}

	}

}
